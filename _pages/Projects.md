---
layout: page
title: Projects
permalink: /projects/
driveId: 1pglD5YtHLuUAYCAz90j_vKorZnOF8mmo/preview
---

## ResBos2

The ResBos2 code is a Monte-Carlo event generator for transverse momentum resummation calculations.
Details about the ResBos2 project can be found on [here](resbos2.gitlab.io).

## Machine Learning

Machine Learning applications to particle physics have become increasingly popular. Many current applications are for
jet physics and separating signal events from background. Below, I discuss projects that I am working on that push
the boundary of machine learning applications in particle physics.

### Phase-Space Integration

With the precision of the LHC, Monte-Carlo Event generators need to produce a similar number of events to mitigate
uncertainties from theory calculations. A major issue with currently, is the required computational costs to acheive
this. In this project, we develop a machine learning alogrithm to improve the efficiency of phase-space integration,
which is the dominate bottleneck in terms of the perturbative calculations.

We use normalizing flows to learn the phase space distribution. The advantage of this technique over previous approaches
is the ability to learn correlations amongst the variables.

{% include googleDrivePlayer.html id=page.driveId %}

<br>

### Uncertainty Quantification

Understanding the uncertainty from the prediction of a neural network is central to using them for scientific research.
In this project, we investigate Bayesian Neural Networks (BNN) and Variational Autoencoders (VAE) to understand the
uncertainty that is propogated from the input date to the extracted results.

### Tracking Applications

## Neutrino Physics

In the study of neutrino physics the dominate uncertainty arises from not understanding the neutrino-nucleus
cross-sections. At the energies of interest at the next generation neutrino experiments DUNE and T2HK, most
interactions occur on nucleons inside the nucleus and not on the nucleus as a whole. Thus it is imperative to model
the path the nucleon takes to escape the nucleus, or known as the cascade. I have developed a new algorithm to
better represent the interactions that occur as the nucleon escapes the nucleus.

## Sub-Leading Color Parton Showers

In parton showers, the calculations are typically done in what is known as leading order color. This neglects
interference terms that appear in the calculations. To fix this, I worked on developing a parton shower that includes
these effects. An implementation of the algorithm can be found [here](https://gitlab.com/isaacson/ColorShower), and details on the project can be found
[here](http://arxiv.org/abs/arXiv:1806.10102).

